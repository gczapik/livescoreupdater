/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven.dataforseo.sampleclient;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Grzesiek
 */
public class SetupTasksRQ {
    
    private Map<String, Task> data = new HashMap<>(); //postId to task
    
    public void put(String postId, Task task){
        data.put(postId, task);
    }

    public Map<String, Task> getData() {
        return data;
    }

    public void setData(Map<String, Task> data) {
        this.data = data;
    }
    
    
}

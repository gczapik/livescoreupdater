/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven.dataforseo.sampleclient;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author Grzesiek
 */
public class DataForSeoSampleClient {
    
    public static void main(String[] args){
        DataForSeoSampleClient client = new DataForSeoSampleClient();
        client.setupTask();
    }
    
    public void setupTask(){
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JacksonJsonProvider.class);
        Client client = Client.create(config);
        client.addFilter(new HTTPBasicAuthFilter("gczapik@gmail.com", "okyJfVmEMvaxkouu"));
        WebResource service = client.resource(UriBuilder.fromUri("https://api.dataforseo.com/").build());
        String responseJson = service.path("v2").path("srp_tasks_post")
                .type("application/json")
                .post(String.class, prepareSampleTaskList());
        System.out.println(responseJson);
    }

    //67793401 - n s 2018
    private SetupTasksRQ prepareSampleTaskList() {
        SetupTasksRQ rq = new SetupTasksRQ();
        Task task = new Task();
        task.setKey("nike sneakers 2017");
        task.setSeId(14);
        task.setLocId(2840);
        rq.put("124", task);
        return rq;
    }
}

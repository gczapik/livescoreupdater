/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven.dataforseo.sampleclient;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Grzesiek
 */
public class Task {
    @JsonProperty("se_id")
    private int seId;
    
    @JsonProperty("loc_id")
    private int locId;
    
    @JsonProperty("key")
    private String key;

    public int getSeId() {
        return seId;
    }

    public void setSeId(int seId) {
        this.seId = seId;
    }

    public int getLocId() {
        return locId;
    }

    public void setLocId(int locId) {
        this.locId = locId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
    
}

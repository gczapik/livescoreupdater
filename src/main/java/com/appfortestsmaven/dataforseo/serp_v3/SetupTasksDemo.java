package com.appfortestsmaven.dataforseo.serp_v3;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.List;

public class SetupTasksDemo {

    private static ObjectMapper mapper = new ObjectMapper();

    public static void main(String[] args) throws URISyntaxException, IOException {
        URI url = new URI("https://api.dataforseo.com/v3/serp/google/organic/task_post");
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);
        String basicAuth = Base64.getEncoder().encodeToString(("gczapik@gmail.com:okyJfVmEMvaxkouu").getBytes("UTF-8"));

        DFSSerpTaskRQV3Producer producer = new DFSSerpTaskRQV3Producer();
        List rq = producer.produceRQArray();

        StringEntity input = new StringEntity(mapper.writeValueAsString(rq));
        input.setContentType("application/json");
        post.setHeader("Content-type", "application/json");
        post.setHeader("Authorization", "Basic " + basicAuth);
        post.setEntity(input);
        HttpResponse pagePostResponse = client.execute(post);
        String jsonRs = EntityUtils.toString(pagePostResponse.getEntity());
        System.out.println("============================================================");
        System.out.println(jsonRs);
        System.out.println("============================================================");
        JSONObject taskPostObj = new JSONObject(jsonRs);
    }
}

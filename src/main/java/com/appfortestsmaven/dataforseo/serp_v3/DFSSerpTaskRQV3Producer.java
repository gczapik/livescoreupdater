package com.appfortestsmaven.dataforseo.serp_v3;

import java.util.ArrayList;
import java.util.List;

public class DFSSerpTaskRQV3Producer {

    public List produceRQArray() {
        List<SerpTaskV3> rq = new ArrayList<>();
        rq.add(produceSampleTask("Fifa 21"));
        return rq;
    }

    private SerpTaskV3 produceSampleTask(String keyword) {
        SerpTaskV3 task = new SerpTaskV3();
        task.setKeyword(keyword);
        task.setDevice("mobile");
        task.setLanguageCode("en");
        task.setLocationCode(2840);
        task.setPriority(1);
        return task;
    }
}

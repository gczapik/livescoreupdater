package com.appfortestsmaven.dataforseo.serp_v3;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SerpTaskV3 {

    @JsonProperty("keyword")
    private String keyword;

    @JsonProperty("priority")
    private int priority;

    @JsonProperty("location_code")
    private int locationCode;

    @JsonProperty("language_code")
    private String languageCode;

    @JsonProperty("device")
    private String device;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(int locationCode) {
        this.locationCode = locationCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}

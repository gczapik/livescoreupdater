package com.appfortestsmaven.dataforseo.relatedkeywords;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class RelatedKeywordsDemo {

    public static void main(String[] args) throws JSONException, IOException, URISyntaxException {
        URI url = new URI("https://api.dataforseo.com/v2/kwrd_finder_related_keywords_get");
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);
        String basicAuth = Base64.getEncoder().encodeToString(("gczapik@gmail.com:okyJfVmEMvaxkouu").getBytes("UTF-8"));

        Map<Integer, Map<String, Object>> postValues = new HashMap<>();

        Random rnd = new Random();
        Map<String, Object> postObj = new HashMap<>();
        postObj.put("keyword", "are hamsters friendly");
        postObj.put("country_code", "US");
        postObj.put("language", "en");
        postObj.put("depth", 2);
        postObj.put("limit", 10);
        postObj.put("offset", 0);
        postObj.put("orderby", "cpc,desc");
//        postObj.put("filters", new Object[]{
//                new Object[]
//                        {"cpc", ">", 0},
//                "or",
//                new Object[]{
//                        new Object[]
//                                {"search_volume", ">", 0},
//                        "and",
//                        new Object[]
//                                {"search_volume", "<=", 1000}
//                }
//        });
        postValues.put(rnd.nextInt(30000000), postObj);
        JSONObject json = new JSONObject().put("data", postValues);
        StringEntity input = new StringEntity(json.toString());

        input.setContentType("application/json");
        post.setHeader("Content-type", "application/json");
        post.setHeader("Authorization", "Basic " + basicAuth);
        post.setEntity(input);
        HttpResponse pagePostResponse = client.execute(post);
        JSONObject taskPostObj = new JSONObject(EntityUtils.toString(pagePostResponse.getEntity()));

        if (taskPostObj.get("status").equals("error") && taskPostObj.isNull("results_count")) {
            System.out.println("error. Code:" + taskPostObj.getJSONObject("error").get("code") + " Message:" + taskPostObj.getJSONObject("error").get("message"));
        } else {
            JSONObject results = taskPostObj.getJSONObject("results");
            Iterator<String> jkeys = results.keys();
            while (jkeys.hasNext()) {
                String key = jkeys.next();
                String status = "";
                if (!results.getJSONObject(key).isNull("status")) {
                    status = results.getJSONObject(key).get("status").toString();
                }
                if (status.equals("error"))
                    System.out.println("Error in task with post_id " + results.getJSONObject(key).get("post_id") + ". Code: " + results.getJSONObject(key).getJSONObject("error").get("code") + " Message: " + results.getJSONObject(key).getJSONObject("error").get("message"));
                else {
                    String responseString = results.getJSONObject(key).toString();
                    System.out.println(responseString);
                    ObjectMapper mapper = new ObjectMapper();
                    RelatedKeywords relatedKeywords = mapper.readValue(responseString, RelatedKeywords.class);
                    relatedKeywords.getRelatedKeywordInfos()
                            .stream()
                            .forEach(keywordInfo -> System.out.println(keywordInfo.getKeyword()+":"+keywordInfo.getSearchVolume()+", competition: "+keywordInfo.getCompetition()));
                }
            }
        }
    }
}

package com.appfortestsmaven.dataforseo.relatedkeywords;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RelatedKeywords {

    @JsonProperty("related")
    private List<RelatedKeywordInfo> relatedKeywordInfos;

    public List<RelatedKeywordInfo> getRelatedKeywordInfos() {
        return relatedKeywordInfos;
    }

    public void setRelatedKeywordInfos(List<RelatedKeywordInfo> relatedKeywordInfos) {
        this.relatedKeywordInfos = relatedKeywordInfos;
    }
}

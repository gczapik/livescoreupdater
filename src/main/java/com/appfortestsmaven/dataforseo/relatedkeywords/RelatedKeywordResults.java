package com.appfortestsmaven.dataforseo.relatedkeywords;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RelatedKeywordResults {

    @JsonProperty("results")
    private Map<String, RelatedKeywords> relatedKeywordResultsMap;

    public Map<String, RelatedKeywords> getRelatedKeywordResultsMap() {
        return relatedKeywordResultsMap;
    }

    public void setRelatedKeywordResultsMap(Map<String, RelatedKeywords> relatedKeywordResultsMap) {
        this.relatedKeywordResultsMap = relatedKeywordResultsMap;
    }
}

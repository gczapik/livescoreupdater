package com.appfortestsmaven.dataforseo.relatedkeywords;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RelatedKeywordInfo {

    @JsonProperty("key")
    private String keyword;

    @JsonProperty("search_volume")
    private Integer searchVolume;

    @JsonProperty("competition")
    private double competition;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getSearchVolume() {
        return searchVolume;
    }

    public void setSearchVolume(Integer searchVolume) {
        this.searchVolume = searchVolume;
    }

    public double getCompetition() {
        return competition;
    }

    public void setCompetition(double competition) {
        this.competition = competition;
    }
}

package com.appfortestsmaven.dataforseo.relatedkeywords;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RelatedKeywordsResponse {

    private RelatedKeywordResults results;

    public RelatedKeywordResults getResults() {
        return results;
    }

    public void setResults(RelatedKeywordResults results) {
        this.results = results;
    }
}

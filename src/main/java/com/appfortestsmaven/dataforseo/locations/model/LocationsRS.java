package com.appfortestsmaven.dataforseo.locations.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationsRS {

    @JsonProperty("tasks_error")
    private int taskErrors;

    @JsonProperty("tasks")
    private List<LocationTask> tasks;

    public int getTaskErrors() {
        return taskErrors;
    }

    public void setTaskErrors(int taskErrors) {
        this.taskErrors = taskErrors;
    }

    public List<LocationTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<LocationTask> tasks) {
        this.tasks = tasks;
    }
}

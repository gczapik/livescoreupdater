package com.appfortestsmaven.dataforseo.locations.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationTask {

    @JsonProperty("result")
    private List<LocationResult> results;

    public List<LocationResult> getResults() {
        return results;
    }

    public void setResults(List<LocationResult> results) {
        this.results = results;
    }
}

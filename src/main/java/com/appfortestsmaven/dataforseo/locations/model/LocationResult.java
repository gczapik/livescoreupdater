package com.appfortestsmaven.dataforseo.locations.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationResult {

    @JsonProperty("location_code")
    private Integer locationCode;

    @JsonProperty("location_name")
    private String locationName;

    @JsonProperty("location_code_parent")
    private Integer locationCodeParent;

    @JsonProperty("country_iso_code")
    private String countryIsoCode;

    @JsonProperty("location_type")
    private String locationType;

    public Integer getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(Integer locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Integer getLocationCodeParent() {
        return locationCodeParent;
    }

    public void setLocationCodeParent(Integer locationCodeParent) {
        this.locationCodeParent = locationCodeParent;
    }

    public String getCountryIsoCode() {
        return countryIsoCode;
    }

    public void setCountryIsoCode(String countryIsoCode) {
        this.countryIsoCode = countryIsoCode;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
}

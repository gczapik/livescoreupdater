package com.appfortestsmaven.dataforseo.locations;

import com.appfortestsmaven.dataforseo.locations.model.LocationsRS;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;

public class LocationsDemo {

    public static void main(String[] args) throws IOException, URISyntaxException {
        URI url = new URI("https://api.dataforseo.com/v3/serp/google/locations");
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet get = new HttpGet(url);
        String basicAuth = Base64.getEncoder().encodeToString(("gczapik@gmail.com:okyJfVmEMvaxkouu").getBytes("UTF-8"));

        get.setHeader("Content-type", "application/json");
        get.setHeader("Authorization", "Basic " + basicAuth);
        HttpResponse pagePostResponse = client.execute(get);
        String jsonRs = EntityUtils.toString(pagePostResponse.getEntity());
        ObjectMapper mapper = new ObjectMapper();
        LocationsRS locationsRS = mapper.readValue(jsonRs, LocationsRS.class);
        System.out.println(locationsRS.getTaskErrors());
    }
}

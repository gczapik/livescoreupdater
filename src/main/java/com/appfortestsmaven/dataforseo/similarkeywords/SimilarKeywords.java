package com.appfortestsmaven.dataforseo.similarkeywords;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimilarKeywords {

    @JsonProperty("similar")
    private List<SimilarKeywordInfo> similarKeywordInfos;

    public List<SimilarKeywordInfo> getSimilarKeywordInfos() {
        return similarKeywordInfos;
    }

    public void setSimilarKeywordInfos(List<SimilarKeywordInfo> similarKeywordInfos) {
        this.similarKeywordInfos = similarKeywordInfos;
    }
}

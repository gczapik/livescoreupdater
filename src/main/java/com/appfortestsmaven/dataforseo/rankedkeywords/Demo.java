package com.appfortestsmaven.dataforseo.rankedkeywords;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.*;

public class Demo {

    public static void main(String[] args) throws IOException, URISyntaxException {
        kwrd_finder_ranked_keywords_get();
    }

    public static void kwrd_finder_ranked_keywords_get() throws JSONException, IOException, URISyntaxException {
        URI url = new URI("https://api.dataforseo.com/v2/kwrd_finder_ranked_keywords_get");
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);
        //Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/login
        String basicAuth = Base64.getEncoder().encodeToString(("gczapik@gmail.com:okyJfVmEMvaxkouu").getBytes("UTF-8"));

        Map<Integer, Map<String, Object>> postValues = new HashMap<>();

        Random rnd = new Random();
        Map<String, Object> postObj = new HashMap<>();
        postObj.put("domain", "petfashionweek.com");
        postObj.put("country_code", "US");
        postObj.put("language", "en");
        postObj.put("limit", 50);
        postObj.put("offset", 0);
        postObj.put("orderby", "search_volume,desc");
        postObj.put("filters", new Object[]{
                new Object[]
                        {"position", "<", 11},
//                "and",
//                new Object[]
//                        {"search_volume", ">=", 10}
        });
        postValues.put(rnd.nextInt(30000000), postObj);
        JSONObject json = new JSONObject().put("data", postValues);
        System.out.println(json.toString());
        StringEntity input = new StringEntity(json.toString());

        input.setContentType("application/json");
        post.setHeader("Content-type", "application/json");
        post.setHeader("Authorization", "Basic " + basicAuth);
        post.setEntity(input);
        HttpResponse pagePostResponse = client.execute(post);
        JSONObject taskPostObj = new JSONObject(EntityUtils.toString(pagePostResponse.getEntity()));

        if (taskPostObj.get("status").equals("error") && taskPostObj.isNull("results_count")) {
            System.out.println("error. Code:" + taskPostObj.getJSONObject("error").get("code") + " Message:" + taskPostObj.getJSONObject("error").get("message"));
        } else {
            JSONObject results = taskPostObj.getJSONObject("results");
            Iterator<String> jkeys = results.keys();
            while (jkeys.hasNext()) {
                String key = jkeys.next();
                String status = "";
                if (!results.getJSONObject(key).isNull("status")) {
                    status = results.getJSONObject(key).get("status").toString();
                }
                if (status.equals("error"))
                    System.out.println("Error in task with post_id " + results.getJSONObject(key).get("post_id") + ". Code: " + results.getJSONObject(key).getJSONObject("error").get("code") + " Message: " + results.getJSONObject(key).getJSONObject("error").get("message"));
                else {
                    ObjectMapper mapper = new ObjectMapper();
                    String responseString = results.getJSONObject(key).toString();
                    RankedKeywordsResponse response = mapper.readValue(responseString, RankedKeywordsResponse.class);
                    System.out.println(responseString);
                    response.getRanked().stream()
                            .forEach(keywordInfo -> System.out.println(keywordInfo.getKey()+":"+keywordInfo.getSearchVolume()+":"+keywordInfo.getPosition()));
                }
            }
        }
    }
}

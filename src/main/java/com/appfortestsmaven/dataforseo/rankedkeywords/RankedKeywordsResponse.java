package com.appfortestsmaven.dataforseo.rankedkeywords;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RankedKeywordsResponse {

    private List<KeywordInfo> ranked;

    public List<KeywordInfo> getRanked() {
        return ranked;
    }

    public void setRanked(List<KeywordInfo> ranked) {
        this.ranked = ranked;
    }
}

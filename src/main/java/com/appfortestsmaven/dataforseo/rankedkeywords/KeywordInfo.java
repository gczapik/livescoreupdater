package com.appfortestsmaven.dataforseo.rankedkeywords;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordInfo {

    private String key;
    @JsonProperty("search_volume")
    private Integer searchVolume;

    @JsonProperty("position")
    private Integer position;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getSearchVolume() {
        return searchVolume;
    }

    public void setSearchVolume(Integer searchVolume) {
        this.searchVolume = searchVolume;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}

package com.appfortestsmaven.dns;



import java.net.InetAddress;
import java.net.UnknownHostException;

import org.xbill.DNS.*;

public class DNSChecker {

    public static void main(String[] args) throws UnknownHostException, TextParseException {
        InetAddress addr = Address.getByName("monitlabs.com");
        System.out.println("OUTPUT: " + addr.getHostAddress());

        Record[] records = new Lookup("monitlabs.com", Type.TXT).run();
        for (int i = 0; i < records.length; i++) {
            TXTRecord txt = (TXTRecord) records[i];
            System.out.println("Name: " + txt.getName() + ", value: " + txt.rdataToString());
        }
    }
}

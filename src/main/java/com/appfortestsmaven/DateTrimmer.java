/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grzesiek
 */
public class DateTrimmer {
    
    public Date trimTime(Date date) {
        try {
            SimpleDateFormat dateWithoutTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateWithoutTimeFormat.parse(dateWithoutTimeFormat.format(date));
        } catch (ParseException ex) {
            Logger.getLogger(DateTrimmer.class.getName()).log(Level.SEVERE, "Parsing date to format has failed", ex);
            return null;
        }
    }

    public Date setLatestPossibleTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    public Date todayMinusDays(int daysBack) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -daysBack);
        return trimTime(cal.getTime());
    }

    public Date nowMinusDays(int daysBack) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -daysBack);
        return cal.getTime();
    }
    
}

package com.appfortestsmaven;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import org.bson.Document;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import static java.util.Calendar.DAY_OF_MONTH;

public class NicheScannerMongoDb {

    public static final String NICHE_REPORT_TASK = "report_task";
    public static final String NICHE_REPORT_KEYWORD = "report_keyword";
    public static final String NICHE_REPORT_GENERIC_SEED_KEYWORD = "generic_seed_keyword";
    public static final String NICHE_REPORT_EXTRA_SERP_RESULT = "extra_serp_result";
    public static final String NICHE_REPORT_ON_PAGE_TASK = "on_page_task";
    public static final String NICHE_REPORT_ON_PAGE_TASK_RESULT = "on_page_task_result";
    public static final String NICHE_REPORT_KEYWORD_SIGNALS_ENTRY = "keyword_signals";
    public static final String NICHE_REPORT_GLOBAL_REPORT_DATA = "global_report_data";
    public static final String NICHE_REPORT_KEYWORD_GROUPS_ENTRY = "keyword_groups";
    public static final String NICHE_REPORT_DOMAIN = "report_domain";
    public static final String NICHE_REPORT_GLOBAL_FORUM_DOMAIN = "forum_domain";
    public static final String NICHE_REPORT_REPORT_FORUM_DOMAIN_SET = "report_forum_domain_set";
    public static final String NICHE_REPORT_USER_FORUM_DOMAIN_SET = "user_forum_domain_set";

    public static void main(String[] args) throws IOException {
//        MongoClient mongoClient = new MongoClient("127.0.0.1", 27017); //local db
        MongoClient mongoClient = new MongoClient("127.0.0.1", 17018); //dev db
//        MongoClient mongoClient = new MongoClient("127.0.0.1", 17017); //tunnel to PROD (small or big -> whatever is online at the moment)
        MongoIterable<String> dbs = mongoClient.listDatabaseNames();
        MongoCursor<String> it = dbs.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
        MongoDatabase database = mongoClient.getDatabase("nichescannerdb");
//        MongoDatabase database = mongoClient.getDatabase("READ_ME_TO_RECOVER_YOUR_DATA");
        Block<Document> printBlock = new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document.toJson());
            }
        };
//        collection.find().forEach(printBlock);
//        System.out.println("===collection count:  "+collection.count());
        MongoCollection<Document> col = database.getCollection(NICHE_REPORT_TASK);
//        MongoCollection<Document> col = database.getCollection("README");
//        groupColl.deleteOne(new Document("_id", "150153840000018"));
        //DELETE ALL======
//        col.deleteMany(new Document());
        //DELETE ALL======
        //=======DELETE SELECTED===========
//        col.deleteMany(prepareQuery());
        //=======DELETE SELECTED===========

        //=======PRINT SELECTED===========
//        col.find(prepareQuery()).forEach(printBlock);
        //=======PRINT SELECTED===========
        //=======PRINT ALL===========
        col.find().forEach(printBlock);
        //=======PRINT ALL===========
//        getRankingEntries(3, 38).forEachRemaining(doc -> System.out.println(doc.toJson()));
        System.out.println("===collection count:  "+col.count());
//        MongoDatabase database = mongoClient.getDatabase("mydb");
//        MongoCollection<Document> collection = database.getCollection("test");
//        System.out.println("Collection count: "+collection.count());
//
////        Document doc = new Document("name", "Grzesiek sample")
////                .append("type", "Grzesiek")
////                .append("count", 1)
////                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
////                .append("info", new Document("x", 203).append("y", 102));
//        Document doc = Document.parse("{\n"
//                + "	title: \"book\", \n"
//                + "	name: \"Grzesiek sample\", \n"
//                + "	pageCount: 346\n"
//                + "}");
//        collection.insertOne(doc);
//
//        Block<Document> printBlock = new Block<Document>() {
//            @Override
//            public void apply(final Document document) {
//                System.out.println(document.toJson());
//            }
//        };
//        FindIterable<Document> col2 = collection.find(eq("name", "Grzesiek sample"));
//        MongoCursor<Document> cursor = col2.iterator();
//        int count=0;
//        while(cursor.hasNext()){
//            count++;
//            cursor.next();
//        }
//
//        System.out.println("Documents which meets specified condition: " + count);
//        col2.forEach(printBlock);
    }

    static BasicDBObject prepareQuery() throws IOException {
        DateTrimmer dateTrimer = new DateTrimmer();
        BasicDBObject query = new BasicDBObject();
        query.put("keywordId", new BasicDBObject("$eq", "1587579870467_355251109"));
//        query.put("_id", new BasicDBObject("$eq", "718cd3b8c19eda6ab852b6abb8b2fdb3"));
//        query.put("dat", new BasicDBObject("$eq", dateTrimer.trimTime(new Date()).getTime()));
//        query.put("dom", new BasicDBObject("$eq", 905));
//        query.put("eng", new BasicDBObject("$eq", "GOOGLE"));
        //==========================
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DAY_OF_MONTH, -2);
//        BasicDBObject query = new BasicDBObject("tfs",
//                new BasicDBObject("$gte", dateTrimmer.trimTime(cal.getTime()).getTime()).append("$lte", dateTrimmer.setLatestPossibleTime(new Date()).getTime()));
//        query.put("key", new BasicDBObject("$eq", 5));

//        BasicDBObject query = new BasicDBObject();
//        query.put("dom", new BasicDBObject("$eq", 3));
//        query.put("eng", new BasicDBObject("$eq", "GOOGLE"));
        //==========================
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DAY_OF_MONTH, -2);
//        BasicDBObject query = new BasicDBObject("dat",
//                new BasicDBObject("$lte", dateTrimmer.trimTime(cal.getTime()).getTime()));
        //==========================
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DAY_OF_MONTH, -1);
//        BasicDBObject query = new BasicDBObject("tfs",
//                new BasicDBObject("$gte", dateTrimmer.trimTime(cal.getTime()).getTime()));
//        query.put("dom", new BasicDBObject("$eq", 905));
        //==========================
//        BasicDBObject query = new BasicDBObject("dat",
//                new BasicDBObject("$gte", todayMinusDays(5).getTime()).append("$lte", todayMinusDays(0).getTime()));
//        query.put("grp", new BasicDBObject("$eq", 1));
//        query.put("dom", new BasicDBObject("$eq", 1467));
        //==========================
//        Date now = new Date();
//        BasicDBObject query = new BasicDBObject("day",
//                new BasicDBObject("$gte", dateTrimer.trimTime(now).getTime()).append("$lte", dateTrimmer.setLatestPossibleTime(now).getTime()));
        //==========================
//        Calendar monthStart = Calendar.getInstance();
//        monthStart.set(DAY_OF_MONTH, 1);
//        Calendar now = Calendar.getInstance();
//        BasicDBObject query = new BasicDBObject("chk",
//                new BasicDBObject("$gte", dateTrimmer.trimTime(monthStart.getTime()).getTime()).append("$lte", now.getTime().getTime()));
//        query.put("key", new BasicDBObject("$eq", 6));
        //==========================
//        BasicDBObject query = new BasicDBObject();
//        query.put("day", new BasicDBObject("$eq", "1526421600000"));
//        query.put("day", new BasicDBObject("$gt", dateTrimer.todayMinusDays(2).getTime()));
//        query.put("eng", new BasicDBObject("$eq", "GOOGLE"));
//        query.put("prj", new BasicDBObject("$eq", 4));
        return query;
    }

    private static Date todayMinusDays(int i) {
        Calendar cal = Calendar.getInstance();
        cal.add(DAY_OF_MONTH, -i);
        return cal.getTime();
    }
}

package com.appfortestsmaven.crawling;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class CrawlingPlayground {

    public static void main(String[] args) throws IOException {
        InputStream in = new URL( "https://www.infotelligent.com/" ).openStream();

        try {
            System.out.println( IOUtils.toString( in ) );
        } finally {
            IOUtils.closeQuietly(in);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven.copies.mongo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author Grzesiek
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordJurnalEntryOld implements IdAwareMongoEntity<String> {

    @JsonProperty("_id")
    private String id;
    
    @JsonProperty("day")
    @JsonDeserialize(using = MongoDateConverter.class)
    private Date day;
    
    @JsonProperty("key")
    private Integer keywordId;
    
    @JsonProperty("rsc")
    @JsonDeserialize(using = MongoLongConverter.class)
    private Long totalResultsCount;
    
    @JsonProperty("trc")
    private boolean tracked;
    
    @JsonProperty("trcDoms")
    private Set<String> trackingDomains;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Integer getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(Integer keywordId) {
        this.keywordId = keywordId;
    }

    public Long getTotalResultsCount() {
        return totalResultsCount;
    }

    public void setTotalResultsCount(Long totalResultsCount) {
        this.totalResultsCount = totalResultsCount;
    }

    public boolean isTracked() {
        return tracked;
    }

    public void setTracked(boolean tracked) {
        this.tracked = tracked;
    }
    
    public Set<String> getTrackingDomains() {
        return trackingDomains;
    }

    public void setTrackingDomains(Set<String> trackingDomains) {
        this.trackingDomains = trackingDomains;
    }

    @Override
    public void calculateId() {
        setId(getDay().getTime() + "_" + getKeywordId());
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven.copies.mongo;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import static com.fasterxml.jackson.databind.node.JsonNodeType.NUMBER;
import java.io.IOException;

/**
 *
 * @author Grzesiek
 */
public class MongoLongConverter extends JsonDeserializer<Long> {

    @Override
    public Long deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.readValueAsTree();
        if(node.getNodeType() == NUMBER){
            return node.asLong();
        }
        return node.get("$numberLong").asLong();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appfortestsmaven;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 *
 * @author Grzesiek
 */
public class LivescoreUpdater {

    private String BASE_URL = "http://www.livescore.in/pl/europa/";

    /**
     *
     * @return all Euro 2016 matches available on livescore.in. Matches are
     * gathered in multimap with date String as a key - used format dd.MM. For
     * example, to get a List with all matches for given date use
     * updater.getMatches().get("21.06.");
     */
    public Multimap<String, Match> getMatches(boolean futureMatches) throws Exception {
        DateFormat format = new SimpleDateFormat("dd.MM.", Locale.US);

//jakby nie dzialalo to odkomentuj te linie i podaj wlasciwa sciezke do swojego firefoxa (nie musi byc prtable)
//        File pathToBinary = new File("D:\\PortableFirefox\\FirefoxPortable\\FirefoxPortable.exe");
//        FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
//        FirefoxProfile firefoxProfile = new FirefoxProfile();
//        FirefoxDriver driver = new FirefoxDriver(ffBinary, firefoxProfile);
        WebDriver driver = new FirefoxDriver();  //to zakomentuj jesli odkomentujesz powyzsze
        driver.get(BASE_URL);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        synchronized (driver) {
            driver.wait(5000);
        }
        BaseFormHandler formHandler = new BaseFormHandler(driver);
        WebElement ancestorDiv = formHandler.findElement(By.id("fscon"));
        if (futureMatches){
            WebElement javascriptLink = findElementToClick(ancestorDiv.findElements(By.tagName("a")));
            javascriptLink.click();
            synchronized (driver) {
                driver.wait(10000);
            }
            ancestorDiv = formHandler.findElement(By.id("fscon"));
        }
        Multimap<String, Match> matches = populateListFromHtmlTableRows(findEuroTable(ancestorDiv.findElement(By.id("fs"))));
        driver.close();
        return matches;
    }

    private WebElement findElementToClick(List<WebElement> links) {
        for (WebElement link : links) {
            if (link.getAttribute("href").equals(BASE_URL + "#") && link.getText().startsWith("Nast") && link.getText().endsWith("pne")) {
                return link;
            }
        }
        return null;
    }

    private WebElement findEuroTable(WebElement ancestorDiv) {
        List<WebElement> tables = ancestorDiv.findElements(By.tagName("table"));
        if (tables.size() == 1) {
            return tables.get(0);
        }
        for (WebElement table : tables) {
            try {
                List<WebElement> spans = table.findElements(By.tagName("span"));
                if (containEuroHeaderElements(spans)) {
                    return table;
                }
            } catch (StaleElementReferenceException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private boolean containEuroHeaderElements(List<WebElement> spans) {
        boolean europaFound = false;
        boolean euroFound = false;
        for (WebElement span : spans) {
            try {
                if (span.getText().replaceAll(" ", "").contains("EUROPA:")) {
                    europaFound = true;
                }
                if (span.getText().replaceAll(" ", "").contains("Euro")) {
                    euroFound = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return euroFound && europaFound;
    }

    private Multimap<String, Match> populateListFromHtmlTableRows(WebElement euroTable) throws ParseException {
        Multimap<String, Match> matches = ArrayListMultimap.create();
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        List<WebElement> rows = euroTable.findElements(By.tagName("tr"));
        for (WebElement row : rows) {
            Match match = createMatchIfApplicableRow(row);
            if (match != null) {
                matches.put(format.format(match.getTime()), match);
            }
        }
        return matches;
    }

    private Match createMatchIfApplicableRow(WebElement row) throws ParseException {
        Match match = new Match();
        Pair<Integer, Integer> score = getScoreFrom(row);
        Date date = getDateFrom(row);
        if (date == null) {
            return null;
        }
        match.setTime(date);
        String homeTeamName = getHomeTeamNameFrom(row);
        if (homeTeamName == null || homeTeamName.isEmpty()) {
            return null;
        }
        Team homeTeam = new Team();
        match.setHomeTeam(homeTeam);
        homeTeam.setName(homeTeamName);
        if (score != null) {
            homeTeam.setGoals(score.getLeft());
        }

        String awayTeamName = getAwayTeamNameFrom(row);
        if (awayTeamName == null || awayTeamName.isEmpty()) {
            return null;
        }
        Team awayTeam = new Team();
        match.setAwayTeam(awayTeam);
        awayTeam.setName(awayTeamName);
        if (score != null) {
            awayTeam.setGoals(score.getRight());
        }
        return match;
    }

    private Date getDateFrom(WebElement row) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy hh:mm", Locale.US);
        List<WebElement> timeCells = row.findElements(By.className("cell_ad"));
        if (timeCells.isEmpty() || timeCells.size() > 1) {
            return null;
        }
        String dateString = timeCells.get(0).getText().split(" ")[0];
        dateString += "2016 " + timeCells.get(0).getText().split(" ")[1];
        return format.parse(dateString);
    }

    private String getHomeTeamNameFrom(WebElement row) {
        List<WebElement> homeTeamCells = row.findElements(By.className("cell_ab"));
        if (homeTeamCells.isEmpty() || homeTeamCells.size() > 1) {
            return null;
        }
        return homeTeamCells.get(0).getText().trim();
    }

    private String getAwayTeamNameFrom(WebElement row) {
        List<WebElement> awayTeamCells = row.findElements(By.className("cell_ac"));
        if (awayTeamCells.isEmpty() || awayTeamCells.size() > 1) {
            return null;
        }
        return awayTeamCells.get(0).getText().trim();
    }

    private Pair<Integer, Integer> getScoreFrom(WebElement row) {
        List<WebElement> scores = row.findElements(By.className("cell_sa"));
        if (scores.isEmpty() || scores.size() > 1) {
            return null;
        }
        String scoreString = scores.get(0).getText().replaceAll(" ", "");
        if (scoreString.contains("(")){
            scoreString = scoreString.split("\\(")[0];
            scoreString = scoreString.substring(0, scoreString.length()-1);
        }
        Pair<Integer, Integer> score = null;
        if (scoreString != null && !scoreString.isEmpty() && scoreString.length() > 2) {
            score = new MutablePair<>(Integer.valueOf(scoreString.split("-")[0]), Integer.valueOf(scoreString.split("-")[1]));
        }
        return score;
    }

    public void addMatches(List<Match> matches) {
        for (Match match : matches) {
            System.out.println("=================================");
            System.out.println(match.getHomeTeam() + "  " + match.getAwayTeam());
            String response = Service.addMatch(match.getTime(),
                    CountryMapper.mapIfNeeded(match.getHomeTeam().getName()),
                    CountryMapper.mapIfNeeded(match.getAwayTeam().getName()));

//            String response = match.getTime()+":"+CountryMapper.mapIfNeeded(match.getHomeTeam().getName())+"-"
//            +CountryMapper.mapIfNeeded(match.getAwayTeam().getName())+" SCORE: "+match.getHomeTeam().getGoals()+":"+match.getAwayTeam().getGoals();
            System.out.println(response);
        }
    }
    
    public void addScores(List<Match> matches) {
        for (Match match : matches) {
            if (match.getHomeTeam().getGoals() != null && match.getAwayTeam().getGoals() != null)
            {
                System.out.println("=================================");
                System.out.println(match.getHomeTeam() + "  " + match.getAwayTeam());
                String response = Service.addScore(match.getTime(),
                    match.getHomeTeam(),
                    match.getAwayTeam());

//                String response = match.getTime() + ":" + CountryMapper.mapIfNeeded(match.getHomeTeam().getName()) + "-"
//                        + CountryMapper.mapIfNeeded(match.getAwayTeam().getName()) + " SCORE: " + match.getHomeTeam().getGoals() + ":" + match.getAwayTeam().getGoals();
                System.out.println(response);
            }
        }
    }

    private static void loadScores(String serviceUrl) throws Exception {
        Service.BASE_URL = serviceUrl;
        LivescoreUpdater lu = new LivescoreUpdater();
        Multimap<String, Match> matchesByDates = lu.getMatches(false);

        for (String day : matchesByDates.keySet()) {
            lu.addScores((List<Match>) matchesByDates.get(day));
        }
    }
    
    public static void loadMatches(String serviceUrl) throws Exception {
        Service.BASE_URL = serviceUrl;
        LivescoreUpdater lu = new LivescoreUpdater();
        Multimap<String, Match> matchesByDates = lu.getMatches(true);

        for (String day : matchesByDates.keySet()) {
            lu.addMatches((List<Match>) matchesByDates.get(day));
        }
    }

    public static void main(String[] args) throws Exception {
//        loadMatches("http://ltxl1681.sgdcelab.sabre.com:8080/service/"); //zaladuj mecze do "Typera" wystawionego pod wskazanym adresem url
        loadScores("http://ltxl1681.sgdcelab.sabre.com:8080/service/"); //załaduj wyniki

        //Uwaga! Jesli Ci nie zadziala bo nie znajdzie FirefoxBinary -> patrz wyzej na komentarz zaczynajacy sie od "jakby nie dzialalo".
    }
}

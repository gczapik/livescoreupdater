/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven.jurnalconverting;

import static com.appfortestsmaven.MongoDBSample.KEYWORD_JURNAL;
import static com.appfortestsmaven.MongoDBSample.URL_COLLECTION;
import com.appfortestsmaven.copies.mongo.KeywordJurnalEntry;
import com.appfortestsmaven.copies.mongo.KeywordJurnalEntryOld;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.toList;
import org.bson.Document;

/**
 *
 * @author Grzesiek
 */
public class KeywordJurnalEntriesConverter {
    
    public static void main(String[] args) throws JsonProcessingException{
        MongoClient mongoClient = new MongoClient("54.39.177.83", 27017);
        MongoDatabase database = mongoClient.getDatabase("monitoprotest1");
        MongoCollection<Document> col = database.getCollection(KEYWORD_JURNAL);
        final List<KeywordJurnalEntryOld> jurnalEntries = new ArrayList<>();
        final ObjectMapper mapper = new ObjectMapper();
//        Block<Document> addToJurnalEntries = new Block<Document>() {
//            @Override
//            public void apply(final Document document) {
//                try {
//                    String json = document.toJson();
//                    KeywordJurnalEntryOld entry = mapper.readValue(json, KeywordJurnalEntryOld.class);
//                    jurnalEntries.add(entry);
//                } catch (IOException ex) {
////                    Logger.getLogger(KeywordJurnalEntriesConverter.class.getName()).log(Level.SEVERE, "failed to map keyword jurnal entry", ex);
//                }
//            }
//        };
//        col.find().forEach(addToJurnalEntries);
//        System.out.println("OLD jurnal entries size: " + jurnalEntries.size());
        
        //=======convert jurnal entries:
//        List<KeywordJurnalEntry> newJurnalEntries = jurnalEntries.stream()
//                .flatMap(oldEntry -> map(oldEntry).stream())
//                .collect(toList());
//        System.out.println("NEW jurnal entries size: " + newJurnalEntries.size());
        
        //=======delete old entries
//        col.deleteMany(new Document());
//        System.out.println("Collection size AFTER deletion of all OLD records: " + col.count());
        
        //=======testing db insertion
//        Document newDoc = mapEntity(newJurnalEntries.get(0), mapper);
//        System.out.println("Collection size BEFORE insert: " + col.count());
//        col.insertOne(newDoc);
//        System.out.println("Collection size AFTER insert: " + col.count());
//        System.out.println("Collection size BEFORE deletion of newly inserted record: " + col.count());
//        col.deleteOne(newDoc);
//        System.out.println("Collection size AFTER deletion of newly inserted record: " + col.count());
        
        //========inserting new collection
//        for(KeywordJurnalEntry toInsert: newJurnalEntries){
//            Document newDoc = mapEntity(toInsert, mapper);
//            col.insertOne(newDoc);
//        }
//        System.out.println("Collection size AFTER insertion of all NEW records: " + col.count());
        
        //========print indexes
        Iterator itInd = col.listIndexes().iterator();
        while (itInd.hasNext()) {
            System.out.println("index: " + itInd.next());
        }
    }
    
    private static List<KeywordJurnalEntry> map(KeywordJurnalEntryOld oldEntry){
        List<KeywordJurnalEntry> newEntries = new ArrayList<>();
        for(String domainName: oldEntry.getTrackingDomains()){
            KeywordJurnalEntry newEntry = new KeywordJurnalEntry();
            newEntry.setDay(oldEntry.getDay());
            newEntry.setKeywordId(oldEntry.getKeywordId());
            newEntry.setTotalResultsCount(oldEntry.getTotalResultsCount());
            newEntry.setTracked(oldEntry.isTracked());
            newEntry.setTrackingDomainName(domainName);
//            problem_z_id_celowa_blokada_kompilacji_patrz_koment_ponizej;
//            newEntry.calculateId(); -> tu id powinno sie przemapowac ze starego id doklejajac do niego nazwe domeny
            newEntries.add(newEntry);
        }
        return newEntries;
    }
    
    private static Document mapEntity(Object entity, ObjectMapper mapper) throws JsonProcessingException {
        String doc = mapper.writeValueAsString(entity);
        return Document.parse(doc);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author Grzesiek
 */
public class BaseFormHandler {

    private WebDriver driver;
    private WebElement form;

    public BaseFormHandler(WebDriver driver) {
        this.driver = driver;
    }

    public void submitForm(By identifierOfSubmitButton) {
        driver.findElement(identifierOfSubmitButton).click();
    }
    
    public void selectOptionByVisibleText(By selectIdentifier, String text) {
        Select select = (Select) findElement(selectIdentifier);
        select.selectByVisibleText(text);
    }

    private List<WebElement> getAllFrames() {
        return driver.findElements(By.tagName("iframe"));
    }

    public WebElement findElement(By elementIdentifier) {
        driver.switchTo().defaultContent();
        WebElement searchedElement = null;
        try {
            searchedElement = driver.findElement(elementIdentifier);
            return searchedElement;
        } catch (Exception e) {
            Logger.getLogger(BaseFormHandler.class.getName()).log(Level.SEVERE, "field not fount in main frame", e);
        }
        if (searchedElement == null) {
            Iterator<WebElement> frames = getAllFrames().iterator();
            while (frames.hasNext() && searchedElement == null) {
                try {
                    driver.switchTo().defaultContent();
                    driver.switchTo().frame(frames.next());
                    searchedElement = driver.findElement(elementIdentifier);
                    return searchedElement;
                } catch (Exception e) {
                    Logger.getLogger(BaseFormHandler.class.getName()).log(Level.SEVERE, "field not fount in given frame", e);
                }
            }
        }
        return null;
    }
    
}

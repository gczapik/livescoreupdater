package com.appfortestsmaven;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;



public class Service {
    
public static String BASE_URL;

 public static String callRest() throws IOException {
  ClientConfig config = new DefaultClientConfig();
  Client client = Client.create(config);
  WebResource service = client.resource(UriBuilder.fromUri(BASE_URL).build());
  
  return service.path("addmatch")
          .queryParam("date", "22.06.2016_21:22")
          .queryParam("country1", "POLAND")
          .queryParam("country2", "GERMANY")
          //.accept(MediaType.TEXT_HTML/*APPLICATION_JSON*/)
          .get(String.class);
  
   }
 
    
    public static String addMatch(Date date, String homeTeam, String awayTeam) {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri(BASE_URL).build());
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy_HH:mm", Locale.ENGLISH);

        return service.path("addmatch")
                .queryParam("date", format.format(date))
                .queryParam("country1", homeTeam)
                .queryParam("country2", awayTeam)
                //                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);
    }
    
    public static String addScore(Date date, Team homeTeam, Team awayTeam) {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri(BASE_URL).build());
        DateFormat format = new SimpleDateFormat("dd.MM", Locale.ENGLISH);

        return service.path("addmatchresult")
                .queryParam("date", format.format(date))
                .queryParam("country1", CountryMapper.mapIfNeeded(homeTeam.getName()))
                .queryParam("country2", CountryMapper.mapIfNeeded(awayTeam.getName()))
                .queryParam("score", homeTeam.getGoals()+":"+awayTeam.getGoals())
                //                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);
    }


    
}

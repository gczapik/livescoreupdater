package com.appfortestsmaven.codingTasks;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static java.util.Arrays.asList;
import static java.util.Collections.sort;
import static java.util.stream.Collectors.toList;

public class Task1 {

    public static String FindIntersection(String[] strArr) {
        String[] strNums1 = strArr[0].replace(" ", "").split(",");
        String[] strNums2 = strArr[0].replace(" ", "").split(",");
        List<Integer> ints1 = mapToInts(strNums1);
        List<Integer> ints2 = mapToInts(strNums2);
        ints1.retainAll(ints2);
        ints2.retainAll(ints1);
        Date d = new Date();

        return generateResult(ints2);


    }

    private static String generateResult(List<Integer> ints2) {
        StringBuilder sb = new StringBuilder();
        boolean firstIteration = true;
        for (Integer integer : ints2) {
            if (!firstIteration) {
                sb.append(", ");
                firstIteration = false;
            }
            sb.append(integer);
        }
        return sb.toString();
    }

    private static List<Integer> mapToInts(String[] strNums) {
        return asList(strNums).stream()
                .map(strNum -> Integer.parseInt(strNum))
                .collect(toList());
    }

    public static void main (String[] args) {
        // keep this function call here
        Scanner s = new Scanner(System.in);
        System.out.print(FindIntersection(new String[]{"1, 3, 4, 7, 13", "1, 2, 4, 13, 15"}));
    }
}

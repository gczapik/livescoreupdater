package com.appfortestsmaven.codingTasks;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Reps {

    public static String LetterCount(String str) {
        if(str == null || str.equals("")) {
            return "-1";
        }
        String[] words = str.split(" ");
        int selectedWordIndex = -1;
        int currMaxReps = 0;
        for(int i=0; i<words.length; i++) {
            int reps = calculateReps(words[i]);
            if (reps > currMaxReps) {
                selectedWordIndex = i;
            }
        }
        return selectedWordIndex != -1 ? words[selectedWordIndex] : "-1";
    }

    private static int calculateReps(String word){
        int reps=0;
        char[] letters = word.toCharArray();
        Set<String> uniques = new HashSet<>();
        for(char letter: letters) {
            boolean added = uniques.add("" + letter);
            if (!added) {
                reps++;
            }
        }
        return reps;
    }

    public static void main (String[] args) {
        // keep this function call here
//        Scanner s = new Scanner(System.in);
        System.out.print(LetterCount("Hello apple pie"));
    }
}

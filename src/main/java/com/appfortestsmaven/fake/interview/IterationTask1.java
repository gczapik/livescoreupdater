package com.appfortestsmaven.fake.interview;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class IterationTask1 {

    public static void main(String[] args) {

        Random rand = new Random();
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            numbers.add(rand.nextInt(100));
        }

        //What will be the result of this? Is it ok?
        for (Integer number : numbers) {
            if (isNotEmpty(number.toString())) {
                numbers.add(100);

            }
        }
    }
}

package com.appfortestsmaven.fake.interview;

import java.util.Optional;

public class OptionalsTask2 {

    //=====================Option 1====================================
    public Optional<SomeClass> proccessSomething(SomeClass arg, String irrelevantArg) {
        //some proccessing code
        arg = meetsCriteria(arg) ? arg : null;
        return Optional.ofNullable(arg);
    }

    //=====================Option 2====================================
    public SomeClass proccessSomething(SomeClass arg) {
        //some proccessing code
        return meetsCriteria(arg) ? arg : null;
    }






























































    private boolean meetsCriteria(SomeClass arg) {
        return false;
    }
}

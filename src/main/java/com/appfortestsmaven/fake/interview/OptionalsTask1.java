package com.appfortestsmaven.fake.interview;

import java.util.Optional;

public class OptionalsTask1 {

    //=====================Option 1====================================
    public void proccessSomething(SomeClass arg) {
        //some proccessing code
    }

    //=====================Option 2====================================
    public void proccessSomething(Optional<SomeClass> arg) {
        //some proccessing code
    }

}

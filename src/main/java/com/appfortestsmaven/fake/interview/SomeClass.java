package com.appfortestsmaven.fake.interview;

public class SomeClass {

    private int order;
    private int orderPlus;

    public SomeClass(int order, int orderPlus) {
        this.order = order;
        this.orderPlus = orderPlus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SomeClass someClass = (SomeClass) o;

        if (order != someClass.order) return false;
        return orderPlus == someClass.orderPlus;
    }

    @Override
    public int hashCode() {
        return order;
    }
}

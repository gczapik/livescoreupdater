package com.appfortestsmaven.fake.interview;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StreamsTask {

    public static void main(String[] args) {
        Random rand = new Random();
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            numbers.add(rand.nextInt(100));
        }
        //Using Java 8 streams (do as a separate tasks):
        //1. collect all numbers that are bigger than 50
        //2. find out how many numbers are bigger than 50 and are odd (nieparzyste)
        //3. sum all the numbers that are odd & bigger than 50
    }
}

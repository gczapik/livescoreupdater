package com.appfortestsmaven.fake.interview;

import java.util.*;

public class IterationTask2 {

    public static void main(String[] args) throws InterruptedException {
        Random rand = new Random();
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            numbers.add(rand.nextInt(100));
        }

        //What will be the result of this? Is it ok?
        Iterator<Integer> it = numbers.iterator();
        while (it.hasNext()) {
            if (it.next() >= 0) {
                it.remove();
            }
        }
//        Thread thread = Thread.currentThread();
//        synchronized (it) {
//            thread.wait(2000);
//        }
        Set<SomeClass> ints = new HashSet<>();
        for (int i=0; i<100; i++) {
            ints.add(new SomeClass(0, i));
        }
        System.out.println("finito");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;

import com.mongodb.client.FindIterable;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import static java.util.Calendar.DAY_OF_MONTH;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.bson.Document;

/**
 *
 * @author Grzesiek
 */
public class MongoDBSample {
    
    public static final String RANKING_ENTRY_COLLECTION = "ranking_entry";
    public static final String PROJECT_RANKING_ENTRY_COLLECTION = "project_ranking_entry";
    public static final String SERP_FEATURE_ENTRY_COLLECTION = "serp_feature";
    public static final String DOMAIN_SERP_FEATURES_COLLECTION = "domain_serp_features";
    public static final String GROUP_AVG_VALUES_COLLECTION = "group_avg_values";
    public static final String DAILY_AVG_RANKING_ENTRY_COLLECTION = "daily_avg";
    public static final String WEEKLY_AVG_RANKING_ENTRY_COLLECTION = "weekly_avg";
    public static final String MONTHLY_AVG_RANKING_ENTRY_COLLECTION = "monthly_avg";
    public static final String KEYWORD_JURNAL = "keyword_jurnal";
    public static final String MONTHLY_DEBUT_COLLECTION = "monthly_keyword_debuts";
    public static final String ABSOLUTE_DEBUT_COLLECTION = "absolute_keyword_debuts";
    public static final String VOLATILITY_COLLECTION = "volatility";
    public static final String FIRST_PAGE_SCORE_COLLECTION = "first_page_score";
    public static final String ALERT_COLLECTION = "alert";
    public static final String PROJECT_DAILY_TRACKED_KEYWORDS_COUNT_COLLECTION = "keywords_tracked_count";
    public static final String KEYWORD_SEARCH_VOLUME = "keyword_search_volume";
    public static final String UI_EVENT_COLLECTION = "uievent_log";
    public static final String SEARCH_VOL_QUERY_JURNAL = "search_vol_query_jurnal";
    public static final String DOMAIN_COLLECTION = "domain_collection";
    public static final String URL_COLLECTION = "url_collection";
    public static final String TOP_SESSION_COUNTS_ENTRIES = "top_session_counts";
    public static final String GSC_CREDENTIALS = "gsc_credentials";
    public static final String DEFAULT_PROJECT_SETTINGS_COLLECTION = "default_project_settings";
    public static final DateTrimmer dateTrimmer = new DateTrimmer();
    
    public static void main(String[] args) throws IOException{
        MongoClient mongoClient = new MongoClient("127.0.0.1", 17017); //ssh tunnel to PROD DB
//        MongoClient mongoClient = new MongoClient("127.0.0.1", 17018); //ssh tunnel to dev DB
//        MongoClient mongoClient = new MongoClient("localhost", 27017);
//        MongoClient mongoClient = new MongoClient("139.99.172.221", 27017);
//        MongoClient mongoClient = new MongoClient("54.39.177.83", 27017); to not compile
        MongoIterable<String> dbs = mongoClient.listDatabaseNames();
        MongoCursor<String> it = dbs.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
        MongoDatabase database = mongoClient.getDatabase("monitlabsdb"); // WCZESNIEJ BYLO monitoprotest1
        MongoCollection<Document> collection = database.getCollection("ranking_entry");
        Iterator itInd = collection.listIndexes().iterator();
        while (itInd.hasNext()) {
            System.out.println("index: "+itInd.next());
        }
        Block<Document> printBlock = new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document.toJson());
            }
        };
//        collection.find().forEach(printBlock);
//        System.out.println("===collection count:  "+collection.count());
        MongoCollection<Document> col = database.getCollection(UI_EVENT_COLLECTION);
//        groupColl.deleteOne(new Document("_id", "150153840000018"));
//        DELETE ALL======
//        col.deleteMany(new Document());
//        DELETE ALL======
        //=======DELETE SELECTED===========
//        col.deleteMany(prepareQuery());
        //=======DELETE SELECTED===========
        
        //=======PRINT SELECTED===========
//        col.find(prepareQuery()).forEach(printBlock);
        //=======PRINT SELECTED===========
        //=======PRINT ALL===========
        col.find().forEach(printBlock);
        //=======PRINT ALL===========
//        getRankingEntries(3, 38).forEachRemaining(doc -> System.out.println(doc.toJson()));
        System.out.println("===collection count:  "+col.count());
//        MongoDatabase database = mongoClient.getDatabase("mydb");
//        MongoCollection<Document> collection = database.getCollection("test");
//        System.out.println("Collection count: "+collection.count());
//        
////        Document doc = new Document("name", "Grzesiek sample")
////                .append("type", "Grzesiek")
////                .append("count", 1)
////                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
////                .append("info", new Document("x", 203).append("y", 102));
//        Document doc = Document.parse("{\n"
//                + "	title: \"book\", \n"
//                + "	name: \"Grzesiek sample\", \n"
//                + "	pageCount: 346\n"
//                + "}");
//        collection.insertOne(doc);
//        
//        Block<Document> printBlock = new Block<Document>() {
//            @Override
//            public void apply(final Document document) {
//                System.out.println(document.toJson());
//            }
//        };
//        FindIterable<Document> col2 = collection.find(eq("name", "Grzesiek sample"));
//        MongoCursor<Document> cursor = col2.iterator();
//        int count=0;
//        while(cursor.hasNext()){
//            count++;
//            cursor.next();
//        }
//        
//        System.out.println("Documents which meets specified condition: " + count);
//        col2.forEach(printBlock);
    }
    
    static BasicDBObject prepareQuery() throws IOException {
        DateTrimmer dateTrimer = new DateTrimmer();
        BasicDBObject query = new BasicDBObject();
        query.put("pix", new BasicDBObject("$ne", null));
        //=================
//        query.put("_id", new BasicDBObject("$eq", "718cd3b8c19eda6ab852b6abb8b2fdb3"));
//        query.put("dat", new BasicDBObject("$eq", dateTrimer.trimTime(new Date()).getTime()));
//        query.put("dom", new BasicDBObject("$eq", 905));
//        query.put("eng", new BasicDBObject("$eq", "GOOGLE"));
        //==========================
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DAY_OF_MONTH, -2);
//        BasicDBObject query = new BasicDBObject("tfs",
//                new BasicDBObject("$gte", dateTrimmer.trimTime(cal.getTime()).getTime()).append("$lte", dateTrimmer.setLatestPossibleTime(new Date()).getTime()));
//        query.put("key", new BasicDBObject("$eq", 5));
        
//        BasicDBObject query = new BasicDBObject();
//        query.put("dom", new BasicDBObject("$eq", 3));
//        query.put("eng", new BasicDBObject("$eq", "GOOGLE"));
        //==========================
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DAY_OF_MONTH, -2);
//        BasicDBObject query = new BasicDBObject("dat",
//                new BasicDBObject("$lte", dateTrimmer.trimTime(cal.getTime()).getTime()));        
        //==========================
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DAY_OF_MONTH, -1);
//        BasicDBObject query = new BasicDBObject("tfs",
//                new BasicDBObject("$gte", dateTrimmer.trimTime(cal.getTime()).getTime()));
//        query.put("dom", new BasicDBObject("$eq", 905));
        //==========================
//        BasicDBObject query = new BasicDBObject("dat",
//                new BasicDBObject("$gte", todayMinusDays(5).getTime()).append("$lte", todayMinusDays(0).getTime()));
//        query.put("grp", new BasicDBObject("$eq", 1));
//        query.put("dom", new BasicDBObject("$eq", 1467));
        //==========================
//        Date now = new Date();
//        BasicDBObject query = new BasicDBObject("day",
//                new BasicDBObject("$gte", dateTrimer.trimTime(now).getTime()).append("$lte", dateTrimmer.setLatestPossibleTime(now).getTime()));
        //==========================
//        Calendar monthStart = Calendar.getInstance();
//        monthStart.set(DAY_OF_MONTH, 1);
//        Calendar now = Calendar.getInstance();
//        BasicDBObject query = new BasicDBObject("chk",
//                new BasicDBObject("$gte", dateTrimmer.trimTime(monthStart.getTime()).getTime()).append("$lte", now.getTime().getTime()));
//        query.put("key", new BasicDBObject("$eq", 6));
        //==========================
//        BasicDBObject query = new BasicDBObject();
//        query.put("day", new BasicDBObject("$eq", "1526421600000"));
//        query.put("day", new BasicDBObject("$gt", dateTrimer.todayMinusDays(2).getTime()));
//        query.put("eng", new BasicDBObject("$eq", "GOOGLE"));
//        query.put("prj", new BasicDBObject("$eq", 4));
        return query;
    }
    
    private static Date todayMinusDays(int i) {
        Calendar cal = Calendar.getInstance();
        cal.add(DAY_OF_MONTH, -i);
        return cal.getTime();
    }
}

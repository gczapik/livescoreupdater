package com.appfortestsmaven.nicheutils;

public enum ReportTaskStatus {

    BLOCKED("BLOCKED"),
    GENERIC_KEYWORD_RESEARCH_IN_PROGRESS("GENERIC_KEYWORD_RESEARCH_IN_PROGRESS"),
    KEYWORD_RESEARCH_IN_PROGRESS("KEYWORD_RESEARCH_IN_PROGRESS"),
    KEYWORD_RESEARCH_DONE("KEYWORD_RESEARCH_DONE"),
    WAITING_FOR_RAW_DATA("WAITING_FOR_RAW_DATA"),
    READY_TO_ANALYSE("READY_TO_ANALYSE"),
    ANALYSING_IN_PROGRESS("ANALYSING_IN_PROGRESS"),
    CRAWLING_AMAZON_PRICES("CRAWLING_AMAZON_PRICES"),
    READY("READY");

    private String name;

    ReportTaskStatus(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

package com.appfortestsmaven.nicheutils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

public class MongoDao {

    private MongoClient client;
    private MongoDatabase database;

    public MongoDao(MongoClient client, MongoDatabase database) {
        this.client = client;
        this.database = database;
    }

    public <T extends Object> List<T> executeQuery(BasicDBObject query, Class<T> type, String collectionName) throws IOException {
        MongoCollection<Document> coll = database.getCollection(collectionName);
        MongoCursor<Document> cursor = coll.find(query).iterator();
        return mapEntities(cursor, type);
    }

    public void insertEntities(Collection entities, String collectionName) throws JsonProcessingException {
        if (isEmpty(entities)) return;
        ObjectMapper mapper = new ObjectMapper();
        List<Document> docs = new ArrayList<>();
        for (Object entity : entities) {
            docs.add(mapEntityInternal(entity, mapper));
        }
        MongoCollection<Document> collectionRaw = database.getCollection(collectionName);
        collectionRaw.insertMany(docs);
    }

    public void insertEntity(Object entity, String collectionName) throws JsonProcessingException {
        MongoCollection<Document> collectionRaw = database.getCollection(collectionName);
        collectionRaw.insertOne(mapEntity(entity));
    }

    public void upsertEntity(IdAwareMongoEntity entity, String collectionName) throws JsonProcessingException {
        MongoCollection<Document> collection = database.getCollection(collectionName);
        entity.calculateId();
        collection.replaceOne(eq("_id", entity.getId()), mapEntity(entity), new UpdateOptions().upsert(true));
    }

    public void remove(BasicDBObject queryFilter, String collectionName) {
        MongoCollection<Document> coll = database.getCollection(collectionName);
        coll.deleteMany(queryFilter);
    }

    private <T extends Object> List<T> mapEntities(MongoCursor<Document> cursor, Class<T> aClass) throws IOException {
        List<T> entities = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        while (cursor.hasNext()) {
            T entity = mapEntityIternal(cursor.next(), aClass, mapper);
            entities.add(entity);
        }
        return entities;
    }

    private Document mapEntity(Object entity) throws JsonProcessingException {
        return mapEntityInternal(entity, new ObjectMapper());
    }

    private Document mapEntityInternal(Object entity, ObjectMapper mapper) throws JsonProcessingException {
        String doc = mapper.writeValueAsString(entity);
        return Document.parse(doc);
    }

    private <T extends Object> T mapEntity(Document doc, Class<T> aClass) throws IOException {
        return mapEntityIternal(doc, aClass, new ObjectMapper());
    }


    private <T extends Object> T mapEntityIternal(Document doc, Class<T> aClass, ObjectMapper mapper) throws IOException {
        String jsonEntry = null;
        try {
            jsonEntry = doc.toJson();
            return mapper.readValue(jsonEntry, aClass);
        } catch (IOException ex) {
            throw new IOException("Error when mapping entity(" + aClass + ") from json Document from entity doc json: \n " + jsonEntry, ex);
        }
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public void setDatabase(MongoDatabase database) {
        this.database = database;
    }

    public MongoDatabase getDatabase() {
        return database;
    }
}

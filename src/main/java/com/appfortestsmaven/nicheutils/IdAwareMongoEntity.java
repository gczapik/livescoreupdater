/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven.nicheutils;

/**
 *
 * @author Grzesiek
 */
public interface IdAwareMongoEntity<T> {
    
    T getId();
    void setId(T id);
    void calculateId();
}

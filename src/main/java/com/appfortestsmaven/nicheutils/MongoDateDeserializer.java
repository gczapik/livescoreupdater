/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven.nicheutils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.Date;

import static com.fasterxml.jackson.databind.node.JsonNodeType.NUMBER;

/**
 *
 * @author Grzesiek
 */
public class MongoDateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.readValueAsTree();
        if (node.getNodeType() == NUMBER) {
            return new Date(node.asLong());
        }
        return new Date(node.get("$numberLong").asLong());
    }
}

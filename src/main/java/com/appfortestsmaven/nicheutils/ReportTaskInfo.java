package com.appfortestsmaven.nicheutils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportTaskInfo implements IdAwareMongoEntity<String> {

    @JsonProperty("_id")
    private String id;

    @JsonProperty("reportName")
    private String reportName;

    @JsonProperty("userId")
    private Integer userId;

    @JsonProperty("added")
    @JsonDeserialize(using = MongoDateDeserializer.class)
    private Date added;

    @JsonProperty("status")
    private String status;

    @JsonProperty("se_id")
    private Integer searchEngineId;

    @JsonProperty("loc_id")
    private Integer locationId;

    @JsonProperty("onPageScanLimit")
    private Integer onPageScanPerKeywordLimit;

    @JsonProperty("seedKeywords")
    private List<String> seedKeywords;

    @JsonProperty("seedDomain")
    private String seedDomain;

    @JsonProperty("maxKeywordsToScan")
    private int maxKeywords;

    @JsonProperty("maxAmazonProductsToScan")
    private int maxAmazonProducts;

    @JsonProperty("costPerKeyword")
    private BigDecimal costPerKeyword;

    @JsonProperty("costPerAmazonProduct")
    private BigDecimal costPerAmazonProduct;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSearchEngineId() {
        return searchEngineId;
    }

    public void setSearchEngineId(Integer searchEngineId) {
        this.searchEngineId = searchEngineId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public void updateStatus(ReportTaskStatus newStatus) {
        setStatus(newStatus.toString());
    }

    public Integer getOnPageScanPerKeywordLimit() {
        return onPageScanPerKeywordLimit;
    }

    public void setOnPageScanPerKeywordLimit(Integer onPageScanPerKeywordLimit) {
        this.onPageScanPerKeywordLimit = onPageScanPerKeywordLimit;
    }

    public List<String> getSeedKeywords() {
        return seedKeywords;
    }

    public void setSeedKeywords(List<String> seedKeywords) {
        this.seedKeywords = seedKeywords;
    }

    public String getSeedDomain() {
        return seedDomain;
    }

    public void setSeedDomain(String seedDomain) {
        this.seedDomain = seedDomain;
    }

    public void setMaxKeywords(int maxKeywords) {
        this.maxKeywords = maxKeywords;
    }

    public int getMaxKeywords() {
        return maxKeywords;
    }

    public void setMaxAmazonProducts(int maxAmazonProducts) {
        this.maxAmazonProducts = maxAmazonProducts;
    }

    public int getMaxAmazonProducts() {
        return maxAmazonProducts;
    }

    public BigDecimal getCostPerKeyword() {
        return costPerKeyword;
    }

    public void setCostPerKeyword(BigDecimal costPerKeyword) {
        this.costPerKeyword = costPerKeyword;
    }

    public BigDecimal getCostPerAmazonProduct() {
        return costPerAmazonProduct;
    }

    public void setCostPerAmazonProduct(BigDecimal costPerAmazonProduct) {
        this.costPerAmazonProduct = costPerAmazonProduct;
    }

    @Override
    public void calculateId() {
        setId(getUserId() + "_" + getAdded().getTime());
    }
}

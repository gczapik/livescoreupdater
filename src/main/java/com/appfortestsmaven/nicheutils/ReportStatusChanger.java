package com.appfortestsmaven.nicheutils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.IOException;

import static com.appfortestsmaven.NicheScannerMongoDb.NICHE_REPORT_TASK;

public class ReportStatusChanger {

    private static final ObjectMapper mapper;
    private static final MongoDao mongoDao;

    static{
        mapper = new ObjectMapper();
        MongoClient mongoClient = new MongoClient("127.0.0.1", 27017); //local db
//        MongoClient mongoClient = new MongoClient("127.0.0.1", 17018); //dev db
//        MongoClient mongoClient = new MongoClient("127.0.0.1", 17017); //tunnel to PROD (small or big -> whatever is online at the moment)
        MongoDatabase database = mongoClient.getDatabase("nichescannerdb");
        MongoCollection<Document> col = database.getCollection(NICHE_REPORT_TASK);
        mongoDao = new MongoDao(mongoClient, database);
    }

    public static void main(String[] args) throws IOException {
        changeStatus("1_1589380300564", ReportTaskStatus.READY_TO_ANALYSE);
    }

    private static void changeStatus(String reportTaskInfoId, ReportTaskStatus newStatus) throws IOException {
        changeStatusInternal(reportTaskInfoId, newStatus);
    }

    private static void changeStatusInternal(String reportTaskInfoId, ReportTaskStatus newStatus) throws IOException {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new BasicDBObject("$eq", reportTaskInfoId));
        ReportTaskInfo reportTaskInfo = mongoDao.executeQuery(query, ReportTaskInfo.class, NICHE_REPORT_TASK).get(0);
        reportTaskInfo.updateStatus(newStatus);
        mongoDao.upsertEntity(reportTaskInfo, NICHE_REPORT_TASK);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven;

import org.openqa.selenium.By;

/**
 *
 * @author Grzesiek
 */
public class HtmlField {

    private By identifier;
    private String value;

    public HtmlField(By identifier, String value) {
        this.identifier = identifier;
        this.value = value;
    }

    public By getIdentifier() {
        return identifier;
    }

    public String getValue() {
        return value;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appfortestsmaven;

import com.appfortestsmaven.copies.mongo.KeywordJurnalEntryOld;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import com.sparkpost.model.AddressAttributes;
import com.sparkpost.model.RecipientAttributes;
import com.sparkpost.model.TemplateContentAttributes;
import com.sparkpost.model.TransmissionWithRecipientArray;
import com.sparkpost.model.responses.Response;
import com.sparkpost.resources.ResourceTransmissions;
import com.sparkpost.transport.RestConnection;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import static java.util.Collections.shuffle;
import static java.util.Collections.sort;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.money.Money;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author Grzesiek
 */
public class DummyMultipurposeClass {
    
    private static final Logger LOGGER = Logger.getLogger(DummyMultipurposeClass.class.getName());
    
    public static void main(String[] args) throws UnsupportedEncodingException, ParseException, SparkPostException, IOException, NoSuchAlgorithmException{
        //================String pool expoeriments START=================
//        String sample = "sample";
//        String sample2 = new String("sample");
//        String sample2 = "sample";
//        System.out.println("====STRINGS EQUAL BY ==: " + (sample == sample2));
        //================String pool expoeriments STOP==================
//        System.out.println(new Date());
        //============URLEncoder START====================
//        String encoded = URLEncoder.encode("/miscellaneous/'is-it-safea-'-leaving-balls-in-car/", StandardCharsets.UTF_8.toString());
//        System.out.println("Encoded: " +encoded);
        //============URLEncoder STOP====================
        //=============STRING COMPARISON START==============
//        compareXStrings(1000, 1000);
        //=============STRING COMPARISON STOP===============
        //=============JODA MONEY START=========
//        Money money = Money.parse("$24.99".replace("$", "USD"));
//        System.out.println("hajs: "+money.getCurrencyUnit()+" "+money.getAmount().toString());
        //=============JODA MONEY STOP==========
//        =================Printing date from milis long=======================
        Date date = new Date();
        //===ranking entries
        date.setTime(1643064578896L);
//        date.setTime(1556773356464L);
        //==datapoints===
//        date.setTime(1556651110145L);
//        date.setTime(1572886554781L);
//        date.setTime(60225854986912L);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm z");
        System.out.println(df.format(date));
//        System.out.println("Date Format: "+df.format(date));
        //=================Hashing performance START===========================
//        List<String> hashes = new ArrayList<>();
//        Date start = new Date();
//        for(int i=0; i<100; i++){
//            String randomString = RandomStringUtils.random(100);//, "qwertyuiopasdfghjklzxcvbnm");
//            String md5Hex = DigestUtils.md5Hex(randomString);
//            hashes.add(md5Hex);
//        }
//        Date stop = new Date();
//        hashes.forEach(h -> System.out.println(h));
//        System.out.println("duration: " + (stop.getTime() - start.getTime()) + " ms.");
        //=================Hashing performance STOP============================
        //=================Hashing sample string START============================
//        System.out.println("hash: "+ DigestUtils.md5Hex("bwvajwmjgnhrdoiu.rand"));
        //=================Hashing sample string STOP=============================
        //=================RAT PASS AND SALT GENERATION START============================
//        MessageDigest md = MessageDigest.getInstance("SHA-256");
//        md.update("lmi8N".getBytes(Charset.forName("UTF-8")));
//        byte[] bytes = md.digest("cMKL34FmDLyb6N9xb6c72D9z2".getBytes(Charset.forName("UTF-8")));
//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < bytes.length; i++) {
//            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
//        }
//        System.out.println("generated hash ->" +sb.toString());
        //=================RAT PASS AND SALT GENERATION STOP============================
//        System.out.println("sum = "+calcMaxFor(100));
//        Date d = new Date();
//        d.setTime(Long.valueOf("1540591200000"));  
//        System.out.println("date = "+d);
//        if(true){
//            return;
//        }
        //=================SORT performance============================
//        List<Date> toLoadClass = produceRandomDatesArrayList(10);
//        sort(toLoadClass);
//        List<Date> datesA = produceRandomDatesArrayList(100000);
//        List<Date> datesL = new LinkedList<>(datesA);
//        Date sortStart = new Date();
//        sort(datesL);
//        System.out.println("LinkedList sort time in milis: "+(new Date().getTime() - sortStart.getTime()));
//        sortStart = new Date();
//        sort(datesA);
//        System.out.println("ArrayList sort time in milis: "+(new Date().getTime() - sortStart.getTime()));
//        datesL.forEach(date -> System.out.println(date));
        //=============================================
        //==========SHUFFLE PERFORMANCE================================
//        List<Date> toLoadClass = produceRandomDatesArrayList(10);
//        shuffle(toLoadClass);
//        List<Date> datesA = produceRandomDatesArrayList(200);
//        List<Date> datesL = new LinkedList<>(datesA);
//        Date shuffleStart = new Date();
//        int i=0;
//        for (i = 0; i < 100000; i++) {
//            shuffle(datesA);
//        }
//        System.out.println("ArrayList shuffle time in milis (" + i + " times): " + (new Date().getTime() - shuffleStart.getTime()));
//        shuffleStart = new Date();
//        for (i = 0; i < 100000; i++) {
//            shuffle(datesL);
//        }
//        System.out.println("LinkedList shuffle time in milis (" + i + " times): " + (new Date().getTime() - shuffleStart.getTime()));
//        //datesL.forEach(date -> System.out.println(date));
        //=============================================
//        WebDriver driver = initFirefoxDriver("89.40.126.40", 30092, true);
//                    WebDriver driver = initHtmlUnitDriver("89.40.126.40", 30092, BrowserVersion.FIREFOX_60);
//                    WebDriver driver = initHtmlUnitDriverNoProxy(BrowserVersion.FIREFOX_60);
//                      openUrlDirectlyByHtmlUnitWebClient("89.40.126.40", 30092, BrowserVersion.FIREFOX_60,
//                              "https://www.google.com/search?q=God%20of%20War%204&num=100&hl=en&gl=US&gws_rd=cr&uule=w+CAIQIFISCQs2MuSEtepUEUK33kOSuTsc");
        openUrlDirectlyByHtmlUnitWebClient(BrowserVersion.FIREFOX_60, "https://www.infotelligent.com/");
//        getAmazonProductPriceUsingHtmlUnit(BrowserVersion.FIREFOX_60,
//                /*base link*/"https://www.amazon.com/Apple-iPhone-Fully-Unlocked-64GB/dp/B077578W38/ref=lp_18637575011_1_1?srs=18637575011&ie=UTF8&qid=1582285885&sr=8-1");
//                /*alternative link*/"https://www.amazon.com/Google-SEO-Bloggers-Optimization-Marketing-ebook/dp/B01N254IGQ/ref=as_li_ss_tl?s=books&ie=UTF8&qid=1485469614&sr=1-1&keywords=google+seo+for+blogger&linkCode=sl1&tag=peerfinance10-20&linkId=a47906c9daaff635e91de3a454bf4c70");
//        openUrlDirectlyByHtmlUnitWebClient(BrowserVersion.FIREFOX_60,
//                "https://www.amazon.com/Apple-iPhone-Fully-Unlocked-64GB/dp/B077578W38/ref=lp_18637575011_1_1?srs=18637575011&ie=UTF8&qid=1582285885&sr=8-1");
//                    driver.get("https://www.onet.pl/");
//        driver.get("https://www.amazon.com/Apple-iPhone-Fully-Unlocked-64GB/dp/B077578W38/ref=lp_18637575011_1_1?srs=18637575011&ie=UTF8&qid=1582285885&sr=8-1");
//        System.out.println("====================RESULT START==================================== ");
//        System.out.println(driver.getPageSource());
//        System.out.println("====================RESULT STOP==================================== ");
//        List<WebElement> results = driver.findElements(By.className("g"));
//        results.forEach(r -> System.out.println(r.getText()));
//        driver.close();
//            //        driver = new FirefoxDriver(firefoxOptions);
//            //        driver.get("https://www.google.com/search?q=God%20of%20War%204&num=100&hl=en&gl=US&gws_rd=cr&uule=w+CAIQIFISCQs2MuSEtepUEUK33kOSuTsc");
//            //        System.out.println("====================RESULT START==================================== ");
//            //        System.out.println(driver.getPageSource());
//            //        System.out.println("====================RESULT STOP==================================== ");
//            //        List<WebElement> links = driver.findElementsByTagName("a");
//            //        WebElement nextPageLink = findNextPageLingByHref(" Najlepsze fiLmy 2017  ", links);
//            //        System.out.println("\n===============\nLink text: "+nextPageLink.getText());
//            //        WebElement div = driver.findElement(By.id("b_tween"));
//            //        WebElement tag = div.findElement(By.className("sb_count"));
//            //        String text = tag.getText();
//            //        int resutsCount = parseTagText(text);
//            //        System.out.println("number = "+resutsCount);
//        ====================================================================
//        =================Date format==================================
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd: hh:mm:ss");
//        System.out.println(format.format(new Date()));
//        ===============Truncating time using date format==================
//        SimpleDateFormat dateWithoutTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = dateWithoutTimeFormat.parse(dateWithoutTimeFormat.format(new Date()));
//        System.out.println(date.getTime());
//        ============Sparkpost hello world======================
//        String API_KEY = "eba7900438552af26b38cdc7875bbb7e3b6a9e57";
//        Client client = new Client(API_KEY);
//        client.sendMessage(
//                "nonexisting@monito24.pl",
//                "gczapik@gmail.com",
//                "Sample message via sparkpost API",
//                "Sample text version message",
//                "<b>Sample HTML version message</b>");
//        -----------
//        sendEmail("test@monito24.pl", new String[]{"gczapik@gmail.com"});
//       ===================================Integer to in comparison=======================
//        BigDecimal bigD = BigDecimal.valueOf(new Integer(13));
//        System.out.println(bigD.intValue());
//        ============================JSON PARSING===========================
//        String json = "{ \"_id\" : \"1531778400000_4\", \"day\" : { \"$numberLong\" : \"1531778400000\" }, \"key\" : 4, \"rsc\" : { \"$numberLong\" : \"7850000000\" }, \"trc\" : true }";
//        ObjectMapper mapper = new ObjectMapper();
//        KeywordJurnalEntryOld entry = mapper.readValue(json, KeywordJurnalEntryOld.class);
//        System.out.println(mapper.writeValueAsString(entry));
//        ==============================BOUNDS TEST==================================
//        Bounds bounds = prepareBoundsForPosition(9);
//        System.out.println("from: " + bounds.getFromPosition() + ", to: " + bounds.getToPosition());
    }
    
    private static int calculatePosBase(double posBaseExact) {
        int posBase = (int) posBaseExact;
        if (posBase == posBaseExact) { //means it origins from positions on max bound (i.e.: 10, 20, 30, etc.) and need to be decremented to go into proper bounds
            posBase--;
        }
        return posBase;
    }

    private static Bounds prepareBoundsForPosition(Integer roundedPos) {
        double posBaseExact = roundedPos / 10.0;
        int posBase = calculatePosBase(posBaseExact);
        return new Bounds((posBase * 10) + 1, (posBase + 1) * 10);
    }

    private static void sendEmail(String from, String[] recipients) throws SparkPostException {
        String API_KEY = "eba7900438552af26b38cdc7875bbb7e3b6a9e57";
        Client client = new Client(API_KEY);
        TransmissionWithRecipientArray transmission = new TransmissionWithRecipientArray();

        // Populate Recipients
        List<RecipientAttributes> recipientArray = new ArrayList<RecipientAttributes>();
        for (String recipient : recipients) {
            RecipientAttributes recipientAttribs = new RecipientAttributes();
            recipientAttribs.setAddress(new AddressAttributes(recipient, "Mark", "Header_to_Mark"));
            recipientArray.add(recipientAttribs);
        }
        transmission.setRecipientArray(recipientArray);

        // Populate Substitution Data
        Map<String, Object> substitutionData = new HashMap<String, Object>();
//        substitutionData.put("name", "Mike");
        substitutionData.put("messagePartOne", "Sample message content for part one or first paragraph or smth like it.<br><br><br>This shoul be 3 lines lower.");

        // Populate Email Body
        TemplateContentAttributes contentAttributes = new TemplateContentAttributes();
        contentAttributes.setTemplateId("mosaico-sample");
//        contentAttributes.setFrom(new AddressAttributes(from, "Grzegorz", "Monito24.pl"));
//        contentAttributes.setSubject("Your subject content here. {{yourContent}}");
//        contentAttributes.setText("Your Text content here.  {{yourContent}}");
//        contentAttributes.setHtml("<p>Your <b>HTML</b> content here.  {{yourContent}}</p>");
        transmission.setSubstitutionData(substitutionData);
        transmission.setContentAttributes(contentAttributes);

        // Send the Email
        RestConnection connection = new RestConnection(client, "https://api.sparkpost.com/api/v1"); //US
//        RestConnection connection = new RestConnection(client, "https://api.eu.sparkpost.com/api/v1"); //EU - requires generation of separate API key
        Response response = ResourceTransmissions.create(connection, 0, transmission);

        LOGGER.debug("Transmission Response: " + response);
    }

    private static WebDriver initFirefoxDriver(String proxyHost, int proxyPort, boolean headless) {
        System.setProperty("webdriver.gecko.driver", "D:\\PortableFirefox\\FirefoxPortable_61\\geckodriver-v0.21.0-win64\\geckodriver.exe");
        //-
//        System.setProperty("webdriver.gecko.driver", ClassLoader.getSystemResource("D:\\PortableFirefox\\FirefoxPortable_61\\geckodriver-v0.21.0-win64\\geckodriver.exe").getFile());
//        System.setProperty("webdriver.firefox.bin", ClassLoader.getSystemResource("D:\\PortableFirefox\\FirefoxPortable_61\\FirefoxPortable\\FirefoxPortable.exe").getFile());
        //-
//        File pathToBinary = new File("D:\\PortableFirefox\\FirefoxPortable_61\\FirefoxPortable\\FirefoxPortable.exe");
        FirefoxBinary ffBinary = new FirefoxBinary();
        if(headless){
            ffBinary.addCommandLineOptions("--headless");
        }
//        ffBinary.addCommandLineOptions("--headless");
//        FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
//        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
//        capabilities.setBrowserName("firefox");
//        capabilities.setVersion("61.0.1");
//        capabilities.setPlatform(Platform.WINDOWS);
//        capabilities.setCapability("marionette", false);
        FirefoxOptions firefoxOptions = new FirefoxOptions();
//        firefoxOptions.merge(capabilities);
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxOptions.setBinary(ffBinary);
        firefoxOptions.setProfile(firefoxProfile);
        firefoxOptions.getProfile().setPreference("network.proxy.type", 1);
        firefoxOptions.getProfile().setPreference("network.proxy.http", proxyHost);
        firefoxOptions.getProfile().setPreference("network.proxy.ssl", proxyHost);
        firefoxOptions.getProfile().setPreference("network.proxy.http_port", proxyPort);
        firefoxOptions.getProfile().setPreference("network.proxy.ssl_port", proxyPort);
        FirefoxDriver driver = new FirefoxDriver(firefoxOptions);
        return driver;
    }

    private static WebDriver initHtmlUnitDriverNoProxy(BrowserVersion browserVersion) {
        HtmlUnitDriver driver = new HtmlUnitDriver(browserVersion, false);
        return driver;
    }

    private static WebDriver initHtmlUnitDriver(String proxyHost, int proxyPort, BrowserVersion browserVersion) {
//        DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
//        capabilities.setVersion("firefox");
        HtmlUnitDriver driver = new HtmlUnitDriver(browserVersion, false);
//        driver.getWebClient().setCssErrorHandler(new SilentCssErrorHandler());
        driver.setProxy(proxyHost, proxyPort);
        return driver;
    }

    private static void openUrlDirectlyByHtmlUnitWebClient(BrowserVersion browserVersion, String url) throws IOException {
        WebClient client = new WebClient(browserVersion);
        client.getOptions().setJavaScriptEnabled(false);
        HtmlPage page = client.getPage(url);
        System.out.println("====================RESULT START==================================== ");
//        System.out.println(page.getWebResponse().getContentAsString());
        page.getAnchors().stream()
                        .forEach(anchor -> System.out.println(anchor.getTextContent()));
        System.out.println("====================RESULT STOP==================================== ");
        List<DomElement> elements = page.getByXPath("//*[@class='" + "g" + "']");
        elements.forEach(e -> System.out.println(e.asText()));
    }

    private static void getAmazonProductPriceUsingHtmlUnit(BrowserVersion browserVersion, String amazonProductUrl) throws IOException {
        WebClient client = new WebClient(browserVersion, "108.59.14.203", 13010);

        HtmlPage page = client.getPage(amazonProductUrl);
        System.out.println("====================RESULT START==================================== ");
        List<DomElement> elements = page.getElementsById("priceblock_ourprice");
        if(isEmpty(elements)){
            elements = page.getByXPath("//span[contains(@class, 'a-color-price')]");
        }
        elements.forEach(e -> System.out.println(e.asText()));
        System.out.println("====================RESULT STOP==================================== ");
        System.out.println(page.getHtmlPageOrNull().getHtmlElementDescendants());
    }

    private static void openUrlDirectlyByHtmlUnitWebClient(String proxyHost, int proxyPort, BrowserVersion browserVersion, String url) throws IOException {
        WebClient client = new WebClient(browserVersion, proxyHost, proxyPort);
        HtmlPage page = client.getPage(url);
        System.out.println("====================RESULT START==================================== ");
        System.out.println(page.getWebResponse().getContentAsString());
        System.out.println("====================RESULT STOP==================================== ");
        List<DomElement> elements = page.getByXPath("//*[@class='" + "g" + "']");
        elements.forEach(e -> System.out.println(e.asText()));
    }
    
    private static int calcMaxFor(int size) {
        int pos=1, sum=0, move=0;
        while(pos*2 <= size+2){
            move = size + 1 - pos;
            sum+=move;
            System.out.println("pos "+pos+": "+move);
            pos++;
        }
        move++;
        while(pos <= size){
            sum += move;
            System.out.println("pos "+pos+": "+move);
            pos++;
        }
        return sum;
    }

    private static int parseTagText(String text) {
        StringBuilder sb = new StringBuilder();
        text = text.replace(" ", "").replace(",", "").replace("\\.", "");
        boolean numberStarted = false;
        for (int i = 0; i < text.length(); i++) {
            String charSring = text.substring(i, i + 1);
            if (StringUtils.isNumeric(charSring)) {
                numberStarted = true;
                sb.append(charSring);
            } else if (numberStarted) {
                break;
            }
        }
        return Integer.valueOf(sb.toString());
    }

    private static WebElement findNextPageLingByHref(String keyword, List<WebElement> links) throws UnsupportedEncodingException {
        keyword = keyword.trim();
        keyword = keyword.toLowerCase();
        while(keyword.contains("  ")){
            keyword = keyword.replace("  ", " ");
        }
        String preparedKeyword = keyword;
        for(WebElement link: links){
            if (containsSearchPhraseAndIsNotNumber(preparedKeyword, link)){
                return link;
            }
        }
        return null;
    }
    
    private static boolean containsSearchPhraseAndIsNotNumber(String keyword, WebElement link) throws UnsupportedEncodingException {
        boolean containsLogo = containsLogo(link);
        boolean containsKeywordQuery = link.getAttribute("href") != null && link.getAttribute("href").contains("/search?q=" + URLEncoder.encode(keyword, "UTF-8"));
        return containsLogo && containsKeywordQuery && isNotInt(link.getText());
    }

    private static boolean isNotInt(String text) {
        try{
            Integer.parseInt(text);
            return false;
        } catch (RuntimeException e){
            return true;
        }
    }

    private static boolean containsLogo(WebElement link) {
        List<WebElement> spans = link.findElements(By.tagName("span"));
        for(WebElement span: spans){
            if(span.getAttribute("style") != null && span.getAttribute("style").contains("nav_logo242.png")){
                return true;
            }
        }
        return false;
    }

    private static List<Date> produceRandomDatesArrayList(int size) {
        List<Date> dates = new ArrayList<>();
        for(int i=0; i<size; i++){
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, new Random().nextInt(1000));
            dates.add(cal.getTime());
        }
        return dates;
    }

    private static void compareXStrings(int list1Limit, int list2Limit){
        List<String> list1 = new ArrayList<>();
        for (int i=0; i<list1Limit; i++){
            list1.add(RandomStringUtils.random(25, "abcdefghijklmnoprstowz"));
        }
        List<String> list2 = new ArrayList<>();
        for (int i=0; i<list2Limit; i++){
            list2.add(RandomStringUtils.random(25, "abcdefghijklmnoprstowz"));
        }
        int comparisons = 0;
        Date start = new Date();
        for (String l1String : list1) {
            for (String l2String : list2) {
                boolean equals = l1String.equals(l2String);
                comparisons++;
            }
        }
        Date stop = new Date();
        System.out.println("time in milis for "+comparisons+" comparisons: "+(stop.getTime() - start.getTime()));
    }

    static class Bounds {

        private final int fromPosition, toPosition;

        public Bounds(int from, int to) {
            this.fromPosition = from;
            this.toPosition = to;
        }

        private int getFromPosition() {
            return fromPosition;
        }

        private int getToPosition() {
            return toPosition;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 79 * hash + this.fromPosition;
            hash = 79 * hash + this.toPosition;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Bounds other = (Bounds) obj;
            if (this.fromPosition != other.fromPosition) {
                return false;
            }
            if (this.toPosition != other.toPosition) {
                return false;
            }
            return true;
        }
    }
}

package com.appfortestsmaven;

/**
 * Created by miras108 on 2016-05-21.
 */
public class CountryMapper {


    public static String mapIfNeeded(String country) {
        switch (country) {
            case "Polska":
                return "POLAND";
            case "Niemcy":
                return "GERMANY";
            case "Francja":
                return "FRANCE";
            case "Rumunia":
                return "ROMANIA";
            case "Albania":
                return "ALBANIA";
            case "Szwajcaria":
                return "SWITZERLAND";
            case "Walia":
                return "WALES";
            case "Słowacja":
                return "SLOVAKIA";
            case "Anglia":
                return "ENGLAND";
            case "Rosja":
                return "RUSSIA";
            case "Turcja":
                return "TURKEY";
            case "Chorwacja":
                return "CROATIA";
            case "Irlandia Północna":
                return "NORTHERN_IRELAND";
            case "Ukraina":
                return "UKRAINE";
            case "Hiszpania":
                return "SPAIN";
            case "Czechy":
                return "CZECH_REPUBLIC";
            case "Irlandia":
                return "REPUBLIC_OF_IRELAND";
            case "Szwecja":
                return "SWEDEN";
            case "Belgia":
                return "BELGIUM";
            case "Włochy":
                return "ITALY";
            case "Austria":
                return "AUSTRIA";
            case "Węgry":
                return "HUNGARY";
            case "Portugalia":
                return "PORTUGAL";
            case "Islandia":
                return "ICELAND";
            default: return country;
        }
    }
}

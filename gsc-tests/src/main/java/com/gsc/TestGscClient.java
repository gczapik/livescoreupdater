package com.gsc;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.MemoryDataStoreFactory;
import com.google.api.services.searchconsole.v1.SearchConsole;
import com.google.api.services.searchconsole.v1.model.SearchAnalyticsQueryRequest;
import com.google.api.services.searchconsole.v1.model.SearchAnalyticsQueryResponse;

import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;

import static com.google.api.services.searchconsole.v1.SearchConsoleScopes.WEBMASTERS_READONLY;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;

public class TestGscClient {

    private static NetHttpTransport TRANSPORT;

    static {
        try {
            TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final JsonFactory JSON_FACTORY = new GsonFactory();
    private static final DataStoreFactory dataStoreFactory = MemoryDataStoreFactory.getDefaultInstance();

    public static void main(String[] args) throws Exception {
        SearchConsole client = new SearchConsole.Builder(
                TRANSPORT,
                 new GsonFactory(),
                authorize())
                .setApplicationName("WebClient")
                .build();
        SearchAnalyticsQueryRequest rq = new SearchAnalyticsQueryRequest()
                .setDimensions(asList("country","device"))
//                .setSearchType()
                .setStartDate("2021-04-01")
                .setEndDate("2021-08-30");
        SearchAnalyticsQueryResponse rs = client.searchanalytics().query("sc-domain:nichescanner.com", rq).execute();
        System.out.println("========================================"+rs.getResponseAggregationType());
    }

    /** Authorizes the installed application to access user's protected data. */
    private static Credential authorize() throws Exception {
        // load client secrets
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
                new InputStreamReader(TestGscClient.class.getResourceAsStream("/client_secrets.json")));
        // set up authorization code flow
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                TRANSPORT, JSON_FACTORY, clientSecrets, singleton(WEBMASTERS_READONLY))
                .setDataStoreFactory(dataStoreFactory)
                .setApprovalPrompt("force")
                .build();
        // authorize
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(56582).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("gczapik@gmail.com");
    }
}
